﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;

namespace Twitter.Client.Services.Interfaces
{
public interface IStreamService
 {
  void Subscribe(ITweetObserver tweetObserver);
  void UnSubscribe(ITweetObserver tweetObserver);
  void NotifyTweetObservers(IAppTweet tweet);
  void StartFetchingTweets(ITwitterCredentials twitterCredentials);
 }
}
