﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;

namespace Twitter.Client.Services.Interfaces
{
 public interface ITweetsMetricsDisplayService
 {
  void Display(ITweetsMetrics tweetsMetrics);
 }
}
