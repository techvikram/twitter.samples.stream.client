﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;

namespace Twitter.Client.Services.Interfaces
{
 public interface ITweetQueueService : ITweetObserver
 {
  bool HasUnreadTweets { get; }
  void AddTweet(IAppTweet tweet);

  IAppTweet GetNextTweet();
 
 }
}
