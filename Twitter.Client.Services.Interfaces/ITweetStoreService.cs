﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Twitter.Client.Models;

namespace Twitter.Client.Services.Interfaces
{
 public interface ITweetStoreService<T> where T: IAppTweet
 {
  void SaveTweet(T tweet);

  IEnumerable<T> GetAllTweets();

  Dictionary<string, int> GetEmojiCounts();

  Dictionary<string, int> GetHashtagCounts();

  Dictionary<string, int> GetUrlDomainCounts();


 }
}
