﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Twitter.Client.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Services.Interfaces;
using Twitter.Client.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Twitter.Client.Services.Tests
{
 [TestClass()]
 public class TweetinviStreamServiceTests
 {

 IStreamService _tweetStreamService;

  public TweetinviStreamServiceTests()
  {
   RegisterServices();
  }

  void RegisterServices()
  {
   // IOC and Dependency Injection using Microsoft.Extensions.DependencyInjection
   var serviceCollection = new ServiceCollection();
   serviceCollection.AddSingleton<IStreamService, TweetinviStreamService>();

   IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
   _tweetStreamService = serviceProvider.GetService<IStreamService>();

   if (serviceProvider != null && serviceProvider is IDisposable)
   {
    ((IDisposable)serviceProvider).Dispose();
   }

  }



  [TestMethod()]
  public void NotifyTweetObserversTest_Subscribe()
  {
   var observer1 = new TestObserver();
   var observer2 = new TestObserver();
   var observer3 = new TestObserver();
   _tweetStreamService.Subscribe(observer1);
   _tweetStreamService.Subscribe(observer2);
   _tweetStreamService.Subscribe(observer3);

   IAppTweet appTweet = new AppTweet();
   _tweetStreamService.NotifyTweetObservers(appTweet);;
   Assert.IsTrue(observer1.ReceivedNotification && observer2.ReceivedNotification && observer3.ReceivedNotification);

  }

  [TestMethod()]
  public void NotifyTweetObserversTest_UnSubscribe()
  {
   var observer1 = new TestObserver();
   var observer2 = new TestObserver();
   _tweetStreamService.Subscribe(observer1);
   _tweetStreamService.Subscribe(observer2);
   _tweetStreamService.UnSubscribe(observer1);
   _tweetStreamService.UnSubscribe(observer2);

   IAppTweet appTweet = new AppTweet();
   _tweetStreamService.NotifyTweetObservers(appTweet); ;
   Assert.IsTrue(!observer1.ReceivedNotification && !observer2.ReceivedNotification);

  }

  protected class TestObserver : ITweetObserver
  {
   // defaults to false
   bool _receivedNotification;

   public bool ReceivedNotification { get => _receivedNotification; }

   public void HandleNewTweet(IAppTweet tweet)
   {
    if (tweet != null) _receivedNotification = true;
   }
  }

 }
}