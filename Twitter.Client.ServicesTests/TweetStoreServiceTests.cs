﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Twitter.Client.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Services.Interfaces;
using Twitter.Client.Models;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Twitter.Client.Services.Tests
{
 [TestClass()]
 public class TweetStoreServiceTests
 {

  ITweetStoreService<IAppTweet> _tweetStoreService;

  public TweetStoreServiceTests()
  {
   RegisterServices();
  }

  void RegisterServices()
  {
   // IOC and Dependency Injection using Microsoft.Extensions.DependencyInjection
   var serviceCollection = new ServiceCollection();
   serviceCollection.AddSingleton<ITweetStoreService<IAppTweet>, TweetStoreService>();

   IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
   _tweetStoreService = serviceProvider.GetService<ITweetStoreService<IAppTweet>>();

   if (serviceProvider != null && serviceProvider is IDisposable)
   {
    ((IDisposable)serviceProvider).Dispose();
   }

  }

  [TestMethod()]
  public void SaveTweetTest_CountAllTweets()
  {
   IAppTweet appTweet = new AppTweet();
   _tweetStoreService.SaveTweet(appTweet);
   appTweet = new AppTweet();
   _tweetStoreService.SaveTweet(appTweet);
   
   IEnumerable<IAppTweet> allTweets = _tweetStoreService.GetAllTweets();
   Assert.IsTrue(allTweets.Count() == 2);

  }


  [TestMethod()]
  public void SaveTweetTest_CountAllEmojis()
  {
   IAppTweet appTweet = new AppTweet();
   appTweet.Emojis.Add("Emoji1");
   appTweet.Emojis.Add("Emoji2");
   _tweetStoreService.SaveTweet(appTweet);
   appTweet = new AppTweet();
   appTweet.Emojis.Add("Emoji1");
   appTweet.Emojis.Add("Emoji3");
   _tweetStoreService.SaveTweet(appTweet);

   var emojiCounts = _tweetStoreService.GetEmojiCounts();
   Assert.IsTrue(emojiCounts["Emoji1"] == 2 && emojiCounts["Emoji2"] == 1 && emojiCounts["Emoji3"] == 1 && emojiCounts.Count == 3);


  }


  [TestMethod()]
  public void SaveTweetTest_CountAllUrls()
  {
   IAppTweet appTweet = new AppTweet();
   appTweet.Urls.Add("https://abc.com/questions");
   appTweet.Urls.Add("https://123.com/questions");
   _tweetStoreService.SaveTweet(appTweet);
   appTweet = new AppTweet();
   appTweet.Urls.Add("https://abc.com/questions");
   appTweet.Urls.Add("https://xyz.org/questions");
   _tweetStoreService.SaveTweet(appTweet);

   var urlCounts = _tweetStoreService.GetUrlDomainCounts();
   Assert.IsTrue(urlCounts["abc.com"] == 2 && urlCounts["123.com"] == 1 && urlCounts["xyz.org"] == 1 && urlCounts.Count == 3);


  }


  [TestMethod()]
  public void SaveTweetTest_CountAllHashtags()
  {
   IAppTweet appTweet = new AppTweet();
   appTweet.Hashtags.Add("ht1");
   appTweet.Hashtags.Add("ht2");
   _tweetStoreService.SaveTweet(appTweet);
   appTweet = new AppTweet();
   appTweet.Hashtags.Add("ht1");
   appTweet.Hashtags.Add("ht3");
   _tweetStoreService.SaveTweet(appTweet);

   var hashtagCounts = _tweetStoreService.GetHashtagCounts();
   Assert.IsTrue(hashtagCounts["ht1"] == 2 && hashtagCounts["ht2"] == 1 && hashtagCounts["ht3"] == 1 && hashtagCounts.Count == 3);

  }

 }
}