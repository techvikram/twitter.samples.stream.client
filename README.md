# twitter.samples.stream.client

Demo app to consume Twitter Sample Stream API and continuously display few aggregate metrics related to the tweet data.

| Service | Overview |
| ------ | ------ |
| IStreamService | Invoke Twitter Sample Stream API using Tweetinvi API. This service implments Observer Patterns to notify observers when new tweet arrives|
| ITweetQueueService | Subscribes to IStreamService and writes the tweets to Queue. Provides interfaces to fetch tweets from queue.  |
| ITweetStoreService| Uses ITweetQueueService to continuosly Scan the queue and Save available tweets in data store. Provides interfaces to Fetch all tweets Data  |
| ITweetsMetricsService|  Continuously scans data store and generates aggregate metrics data  |
| ITweetsMetricsDisplayService | Renders aggregate metrics data on to UI  |

# Interface Specifications
![alt text](images/ClassDiagram.png "Interfaces")

# Example output
![alt text](images/SampleOutput.PNG "Sample Output")


