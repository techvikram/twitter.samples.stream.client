﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Models.Entities;
using Tweetinvi.Streaming;
using Twitter.Client.Models;
using ITwitterCredentials = Twitter.Client.Models.ITwitterCredentials;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client.Services
{
 /// <summary>
 /// Observer Pattern to notify subscribers when new tweet arrives
 /// </summary>
 public class TweetinviStreamService : IStreamService
 {
  private List<ITweetObserver> _tweetObservers;
  private ISampleStream _sampleStream;
  public TweetinviStreamService()
  {
   this._tweetObservers = new List<ITweetObserver>();
  }

  public void NotifyTweetObservers(IAppTweet tweet)
  {
   foreach(ITweetObserver tweetObserver in this._tweetObservers)
   {
     tweetObserver.HandleNewTweet(tweet);
   }
  }

  protected  void HandleNewTweet(ITweet tweet)
  {
   List<string> urls = tweet.Urls.ConvertAll(x => x.ExpandedURL);
   List<string> hashtags = tweet.Hashtags.ConvertAll(x => x.Text);
   List<string> symbols = tweet.Entities.Symbols.ConvertAll(x => x.Text);
   bool hasPic = tweet.Entities.Medias.Exists(x => x.MediaType == "photo");
   IAppTweet appTweet = new AppTweet(text: tweet.Text, url: tweet.Url, urls: urls, hashtags: hashtags, emojis: symbols, hasPic: hasPic );
   this.NotifyTweetObservers(appTweet);
  }

  public void Subscribe(ITweetObserver tweetObserver)
  {
   this._tweetObservers.Add(tweetObserver);
  }

  public void UnSubscribe(ITweetObserver tweetObserver)
  {
   this._tweetObservers.Remove(tweetObserver);
  }

  public async void StartFetchingTweets(ITwitterCredentials twitterCredentials)
  {
   if(_sampleStream == null)
   {

    var userClient = new TwitterClient(twitterCredentials.ConsumerKey, twitterCredentials.ConsumerSecret, twitterCredentials.AccessToken, twitterCredentials.AccessTokenSecret);
    _sampleStream = userClient.Streams.CreateSampleStream();
   }

   _sampleStream.TweetReceived += (sender, eventArgs) =>
   {
    this.HandleNewTweet(eventArgs.Tweet);
   };

   await _sampleStream.StartAsync();
  }
 }
}
