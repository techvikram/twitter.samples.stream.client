﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Twitter.Client.Models;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client.Services
{
 public class TweetsMetricsService : ITweetsMetricsService
 {

  ITweetsMetrics _tweetsMetrics;

  public TweetsMetricsService(ITweetsMetrics tweetsMetrics)
  {
   _tweetsMetrics = tweetsMetrics;
  }
  public ITweetsMetrics GetTweetsMetrics(ITweetStoreService<IAppTweet> tweetStoreService)
  {
   _tweetsMetrics.TopDomains = GetTopFive(tweetStoreService.GetUrlDomainCounts());
   _tweetsMetrics.TopHashtags = GetTopFive(tweetStoreService.GetHashtagCounts());
   _tweetsMetrics.TopEmojis = GetTopFive(tweetStoreService.GetEmojiCounts());

   IEnumerable<IAppTweet> allTweets = tweetStoreService.GetAllTweets();
   allTweets = new List<IAppTweet>(allTweets);

   _tweetsMetrics.Total = allTweets.Count();
   if (_tweetsMetrics.Total > 0)
   {
    _tweetsMetrics.PercentUrlTweets = ((allTweets.Where(x => x.HasUrl).Count() * 1.0) / _tweetsMetrics.Total) * 100;
    _tweetsMetrics.PercentEmojiTweets = ((allTweets.Where(x => x.HasEmoji).Count() * 1.0) / _tweetsMetrics.Total) * 100;
    _tweetsMetrics.PercentPhotoUrlTweets = ((allTweets.Where(x => x.HasPic).Count() * 1.0)/ _tweetsMetrics.Total) * 100;
    int tweetsByHour = allTweets.GroupBy(x => new { x.RecievedDT.Date, x.RecievedDT.Hour } ).Count();
    int tweetsByMin= allTweets.GroupBy(x => x.RecievedDT.ToString("f")).Count();
    int tweetsBySec = allTweets.GroupBy(x => x.RecievedDT.ToString("F")).Count();
    if (tweetsByHour > 0)
     _tweetsMetrics.AveragePerHour = _tweetsMetrics.Total / tweetsByHour;
    if (tweetsByMin > 0)
     _tweetsMetrics.AveragePerMinute = _tweetsMetrics.Total / tweetsByMin;
    if (tweetsBySec > 0)
     _tweetsMetrics.AveragePerSecond = _tweetsMetrics.Total / tweetsBySec;
   }
  
   return _tweetsMetrics;
  }

  IEnumerable<KeyValuePair<string, int>> GetTopFive(Dictionary<string, int> dict)
  {
   dict = new Dictionary<string, int>(dict);
   return dict.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value).Take(5);
  }
 }
}
