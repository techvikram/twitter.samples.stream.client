﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client.Services
{

 public class TweetStoreService : ITweetStoreService<IAppTweet>
 {
  List<IAppTweet> _tweets;
  Dictionary<string, int> _emojiCounts;
  Dictionary<string, int> _hashtagCounts;
  Dictionary<string, int> _urlDomainCounts;

  public TweetStoreService()
  {
   _tweets = new List<IAppTweet>();
   _emojiCounts = new Dictionary<string, int>();
   _hashtagCounts = new Dictionary<string, int>();
   _urlDomainCounts = new Dictionary<string, int>();
  }

  public IEnumerable<IAppTweet> GetAllTweets()
  {
   return _tweets;
  }

  public Dictionary<string, int> GetEmojiCounts()
  {
   return _emojiCounts;
  }

  public Dictionary<string, int> GetHashtagCounts()
  {
   return _hashtagCounts;
  }

  public Dictionary<string, int> GetUrlDomainCounts()
  {
   return _urlDomainCounts;
  }

  public void SaveTweet(IAppTweet tweet)
  {
   _tweets.Add(tweet);
   if(tweet.Emojis != null) UpdateDictionary(_emojiCounts, tweet.Emojis);
   if (tweet.Hashtags != null) UpdateDictionary(_hashtagCounts, tweet.Hashtags);
   if (tweet.Urls != null) UpdateDictionary(_urlDomainCounts, tweet.Urls.ConvertAll(x =>{
    Uri myUri = new Uri(x);
    return myUri.Host; 
   }));
  }

  protected void UpdateDictionary(Dictionary<string, int> dictionary, List<string> newValues)
  {
   foreach (string val in newValues)
   {
    if (dictionary.ContainsKey(val))
    {
     dictionary[val] += 1;
    }
    else
    {
     dictionary.Add(val, 1);
    }
   }
  }
 }
}
