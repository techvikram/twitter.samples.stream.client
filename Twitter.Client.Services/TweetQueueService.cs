﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Twitter.Client.Models;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client.Services
{
 public class TweetQueueService : ITweetQueueService
 {

 private Queue<IAppTweet> _tweetQueue;
  public TweetQueueService()
  {
   _tweetQueue = new Queue<IAppTweet>();
  }

  public bool HasUnreadTweets { 
   get { return _tweetQueue.Count > 0;  }
  }

  public async void AddTweet(IAppTweet tweet)
  {
   await Task.Run(() =>
   {
    _tweetQueue.Enqueue(tweet);
   });
  }

  public IAppTweet GetNextTweet()
  {
   return _tweetQueue.Dequeue();
  }

  public async void HandleNewTweet(IAppTweet tweet)
  {
   await Task.Run(() =>
   {
    this.AddTweet(tweet);
   });
  }
 }
}
