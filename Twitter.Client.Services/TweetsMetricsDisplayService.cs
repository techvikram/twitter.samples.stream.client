﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client.Services
{
 public class TweetsMetricsDisplayService : ITweetsMetricsDisplayService
 {
  public void Display(ITweetsMetrics tweetsMetrics)
  {
   Console.WriteLine("***************************************");
   Console.WriteLine(String.Format("Total Tweets: {0}", tweetsMetrics.Total));
   Console.WriteLine();
   Console.WriteLine(String.Format("Tweets per Second: {0}", tweetsMetrics.AveragePerSecond));
   Console.WriteLine(String.Format("Tweets per Minute: {0}", tweetsMetrics.AveragePerMinute));
   Console.WriteLine(String.Format("Tweets per Hour: {0}", tweetsMetrics.AveragePerHour));
   Console.WriteLine();
   Console.WriteLine("***************************************");
   Console.WriteLine(String.Format("Percent of tweets that contains emojis: {0}", Math.Round(tweetsMetrics.PercentEmojiTweets, 2)));
   Console.WriteLine(String.Format("Percent of tweets that contain a url: {0}", Math.Round(tweetsMetrics.PercentUrlTweets, 2)));
   Console.WriteLine(String.Format("Percent of tweets that contain a photo url: {0}", Math.Round(tweetsMetrics.PercentPhotoUrlTweets, 2)));
   Console.WriteLine();
   Console.WriteLine("*************Top Emojis**************");
   Console.WriteLine(FormatTopsForDisplay(tweetsMetrics.TopEmojis));
   Console.WriteLine();
   Console.WriteLine("*************Top Hastags**************");
   Console.WriteLine(FormatTopsForDisplay(tweetsMetrics.TopHashtags));
   Console.WriteLine();
   Console.WriteLine("*************Top Domains**************");
   Console.WriteLine(FormatTopsForDisplay(tweetsMetrics.TopDomains));
  }

  string FormatTopsForDisplay(IEnumerable<KeyValuePair<string, int>> dict)
  {
   StringBuilder content = new StringBuilder();
   foreach(KeyValuePair<string, int> item in dict)
   {
    content.AppendLine(String.Format("{0} : {1}", item.Key, item.Value));
   }
   return content.ToString();
  }
 }
}
