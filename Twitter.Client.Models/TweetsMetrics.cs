﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twitter.Client.Models
{
public  class TweetsMetrics : ITweetsMetrics
 {

  public TweetsMetrics()
  {
   TopHashtags = new Dictionary<string, int>();
   TopEmojis = new Dictionary<string, int>();
   TopDomains = new Dictionary<string, int>();
  }

  public int Total { get; set; }

  public int AveragePerSecond { get; set; }

  public int AveragePerMinute { get; set; }

  public int AveragePerHour { get; set; }

  public double PercentEmojiTweets { get; set; }

  public double PercentUrlTweets { get; set; }

  public double PercentPhotoUrlTweets { get; set; }

  public IEnumerable<KeyValuePair<string, int>> TopHashtags { get; set; }

  public IEnumerable<KeyValuePair<string, int>> TopEmojis { get; set; }

  public IEnumerable<KeyValuePair<string, int>> TopDomains { get; set; }

 }
}
