﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twitter.Client.Models
{
 public class AppTweet : IAppTweet
 {

  protected string _text;

  protected List<string> _hashtags;

  protected List<string> _urls;

  protected List<string> _emojis;

  protected bool _hasPic;

  protected string _url;

  protected DateTime _recievedDT;

  public AppTweet()
  {
   this._recievedDT = DateTime.Now;
   this._hashtags = new List<string>();
   this._urls = new List<string>();
   this._emojis = new List<string>();
  }

  public AppTweet(string text, List<string> hashtags, List<string> urls, string url, List<string> emojis, bool hasPic)
  {
   this._recievedDT = DateTime.Now;
   this._text = text;
   this._hashtags = new List<string>(hashtags);
   this._urls = new List<string>(urls);
   this._url = url;
   this._hasPic = hasPic;
   this._emojis = new List<string>(emojis);

  }

  public DateTime RecievedDT { get => _recievedDT; set => _recievedDT = value; }

  //
  // Summary:
  //     Formatted text of the tweet.
  public string Text { get => _text; set => _text = value; }

  //
  // Summary:
  //     Collection of hashtags associated with a Tweet
  public List<string> Hashtags { get => _hashtags; set => _hashtags = new List<string>(value); }

  //
  // Summary:
  //     Collection of urls associated with a tweet
  public List<string> Urls { get => _urls; set => _urls = new List<string>(value); }

  public List<string> Emojis { get => _emojis; set => _emojis = new List<string>(value); }

  public bool HasEmoji { get => _emojis != null && _emojis.Count > 0; }

 public  bool HasUrl { get => _urls != null && _urls.Count > 0; }

  public bool HasPic { get => _hasPic; set => _hasPic = value; }

  //
  // Summary:
  //     URL of the tweet on twitter.com
  public string Url { get => _url; set => _url = value; }


 }
}
