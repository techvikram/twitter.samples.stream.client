﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twitter.Client.Models
{
 public interface IAppTweet
 {
  DateTime RecievedDT { get; set; }
  //
  // Summary:
  //     Formatted text of the tweet.
  string Text { get; set; }

  //
  // Summary:
  //     Collection of hashtags associated with a Tweet
  List<string> Hashtags { get; set;  }

  //
  // Summary:
  //     Collection of urls associated with a tweet
  List<string> Urls { get; set; }

  List<string> Emojis { get; set;  }

  bool HasEmoji { get; }

  bool HasUrl { get; }

  bool HasPic { get; set; }

  //
  // Summary:
  //     URL of the tweet on twitter.com
  string Url { get; set; }

 }
}
