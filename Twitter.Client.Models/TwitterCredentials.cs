﻿using System;
using System.Collections.Generic;
using System.Text;
using Twitter.Client.Models;

namespace Twitter.Client.Services
{
 public class TwitterCredentials : ITwitterCredentials
 {

  public readonly string _consumerKey;

  public readonly string _consumerSecret;

  public readonly string _accessToken;

  public readonly string _accessTokenSecret;

  public TwitterCredentials(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
  {
   this._consumerKey = consumerKey;
   this._consumerSecret = consumerSecret;
   this._accessToken = accessToken;
   this._accessTokenSecret = accessTokenSecret;

  }

  public string ConsumerKey => _consumerKey;

  public string ConsumerSecret => _consumerSecret; 

  public string AccessToken => _accessToken; 

  public string AccessTokenSecret => _accessTokenSecret; 
 }
}
