﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twitter.Client.Models
{
 public interface ITwitterCredentials
 {
  public string ConsumerKey { get;  }
  public string ConsumerSecret { get; }
  public string AccessToken { get; }
  public string AccessTokenSecret { get; }
 }
}
