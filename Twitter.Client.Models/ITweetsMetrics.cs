﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twitter.Client.Models
{
 public interface ITweetsMetrics
 {
  int Total { get; set; }

  int AveragePerSecond { get; set; }

  int AveragePerMinute { get; set; }

  int AveragePerHour { get; set; }

  double PercentEmojiTweets { get; set; }

  double PercentUrlTweets { get; set; }

  double PercentPhotoUrlTweets { get; set; }

  IEnumerable<KeyValuePair<string, int>> TopHashtags { get; set; }

  IEnumerable<KeyValuePair<string, int>> TopEmojis { get; set; }

  IEnumerable<KeyValuePair<string, int>> TopDomains { get; set; }
}
}
