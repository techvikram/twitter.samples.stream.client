﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Twitter.Client.Models;
using Twitter.Client.Services;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client
{
 public class TwitterClient
 {
  const string CONSUMER_KEY = "REMOVED";
  const string CONSUMER_SECRET = "REMOVED";
  const string ACCESS_TOKEN = "REMOVED";
  const string ACCESS_TOKEN_SECRET = "REMOVED";

  IStreamService _streamService;
  ITweetQueueService _tweetQueueService;
  ITweetStoreService<IAppTweet> _tweetStoreService;
  ITweetsMetricsService _tweetsMetricsService;
  ITweetsMetricsDisplayService _tweetsMetricsDisplayService;

  /// <summary>
  /// Runtime Constructor injection of concrete types
  /// </summary>
  public TwitterClient(IStreamService streamService, 
   ITweetQueueService tweetQueueService, 
   ITweetStoreService<IAppTweet> tweetStoreService, 
   ITweetsMetricsService tweetsMetricsService,
   ITweetsMetricsDisplayService tweetsMetricsDisplayService)
  {
   this._streamService = streamService;
   this._tweetQueueService = tweetQueueService;
   this._tweetStoreService = tweetStoreService;
   _streamService.Subscribe(_tweetQueueService);
   _tweetsMetricsService = tweetsMetricsService;
   _tweetsMetricsDisplayService = tweetsMetricsDisplayService;
  }

  /// <summary>
  ///  Runs three parallel tasks, nonblocking async tasks
  ///  Task1: Fetches the Data from Sample Stream API and dumps the responses in Queue
  ///  Task2: Continuously processes the queue and places the data in data store
  ///  Task3: Continuously processes the data store, generates metrics and diplays metric
  /// </summary>
  public void Run()
  {

   Parallel.Invoke(
    () => _streamService.StartFetchingTweets(new TwitterCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)),
    () =>
    {
     while (true)
     {
      Thread.Sleep(2000);
      while (_tweetQueueService.HasUnreadTweets)
      {
       IAppTweet tweet = _tweetQueueService.GetNextTweet();
       _tweetStoreService.SaveTweet(tweet);
      }
     }
    },
    () =>
    {
     while (true)
     {
      Thread.Sleep(5000);
      Console.Clear(); 
      Console.WriteLine("Screen refreshes every 5 seconds");
      ITweetsMetrics tweetsMetrics = _tweetsMetricsService.GetTweetsMetrics(_tweetStoreService);
      _tweetsMetricsDisplayService.Display(tweetsMetrics);
     }
    }
    );

   Console.ReadLine();
  }

 }
}
