﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Twitter.Client.Models;
using Twitter.Client.Services;
using Twitter.Client.Services.Interfaces;

namespace Twitter.Client
{
 class Program
 {
  static void Main(string[] args)
  {

   // IOC and Dependency Injection using Microsoft.Extensions.DependencyInjection
   // Register Service implentations
   var serviceCollection = new ServiceCollection();
   ConfigureServices(serviceCollection);

   IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
   IServiceScope scope = serviceProvider.CreateScope();
   scope.ServiceProvider.GetRequiredService<TwitterClient>().Run();
   if (serviceProvider != null && serviceProvider is IDisposable)
   {
    ((IDisposable)serviceProvider).Dispose();
   }

  }

  private static void ConfigureServices(IServiceCollection serviceCollection)
  {
   serviceCollection.AddSingleton<ITweetQueueService, TweetQueueService>();
   serviceCollection.AddSingleton<IStreamService, TweetinviStreamService>();
   serviceCollection.AddSingleton<ITweetStoreService<IAppTweet>, TweetStoreService>();
   serviceCollection.AddTransient<ITweetsMetrics, TweetsMetrics>();
   serviceCollection.AddSingleton<ITweetsMetricsService, TweetsMetricsService>();
   serviceCollection.AddSingleton<ITweetsMetricsDisplayService, TweetsMetricsDisplayService>();

   serviceCollection.AddSingleton<TwitterClient>();

  }
 }
}
